package Test;

import Utils.ItsNecesaryReview.Secuencia;

public class TestSecuencia {
    public static void main(String[] args) {

        // Creating
        int[] vector1 = createVector(5);
        int[] vector2 = createVector(10);

        // Creating Secuencia
        Secuencia<Integer> secuencia1 = new Secuencia<Integer>(vector1.length);
        Secuencia<Integer> secuencia2 = new Secuencia<Integer>(vector2.length);

        // Equaling secuencia Vector to vector
        for (int i = 0; i < vector1.length; i++) {
            secuencia1.add(i, vector1[(vector1.length - 1) - i]);
        }
        for (int i = 0; i < vector2.length; i++) {
            secuencia2.add(i, vector2[i]);
        }

        //Printing and testing
        System.out.println("\nSecuencia1 without sort:\n\t" + secuencia1);
        System.out.println("Secuencia2 without sort:\n\t" + secuencia2 + "\n");

        //Sorting
        secuencia1.sort();
        secuencia2.sort();
        System.out.println("\nSecuencia1 sorted:\n\t" + secuencia1);
        System.out.println("Secuencia2 sorted:\n\t" + secuencia2 + "\n");

        //Uning
        Secuencia<Integer> secUnion = secuencia1.getUnion(secuencia2);
        System.out.println("\nUnion between 1 and 2:\n\t" + secUnion + "\n");

        //Instersecting
        Secuencia<Integer> secIntersect = secuencia1.getIntersect(secuencia2);
        System.out.println("\nIntersection between 1 and 2:\n\t" + secIntersect + "\n");

        //Diference
        Secuencia<Integer> secDiference = secuencia1.getDiference(secuencia2);
        System.out.println("\nDiference between 1 and 2:\n\t" + secDiference + "\n");

        //Simetrical Diference
        Secuencia<Integer> secSimDif = secuencia1.getSimetricDiference(secuencia2);
        System.out.println("\nSimetrical Diference between 1 and 2:\n\t" + secSimDif + "\n");

        //Even Sets
        System.out.println("\nPrinting Even sets from secuencia1:");
        secuencia1.getEvenSets();
    }

    // Method for create a vector from 0 to i positions
    public static int[] createVector(int i) {
        int[] vector = new int[i];
        for (int j = 0; j < vector.length; j++) {
            vector[j] = j * i + 10;
        }
        return vector;
    }
}
