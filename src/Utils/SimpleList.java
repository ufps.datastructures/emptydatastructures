package Utils;

/**
 * This class model a List of generic objects using Nodos, where each Nodo has a
 * Object inside
 * 
 * @author Adolfo Alejandro Arenas Ramos
 * @param T The class of Objects that this List will save
 */
@SuppressWarnings("rawtypes")
public class SimpleList<T extends Comparable> {

    // SortMaxMin()

    /**
     * The head of the List will be the first element of the list
     */
    private Nodo<T> head;

    /**
     * The Size of our element will across from the head to null
     */
    private int size;

    /**
     * Generic empty constructor for Lista where the size initialize in 0
     */
    public SimpleList() {
        this.size = 0;
    }

    /*
     * This enums defines if the list will be sorted or if the elements will be
     * added in an Ascendent, Descendent, of None order
     */
    public enum SortedBy {
        ASCENDENT, DESCENDENT
    }

    /**
     * This method obtains the object contained by the Node in the position 'i' of
     * the
     * Lista
     * 
     * @param i Is the number of the position we wanna find
     * @return Return the generic object we wanna obtain
     */
    public T get(int i) {
        validatePosition(i);
        return this.getPos(i).getInfo();
    }

    /**
     * Sets the value of the object at the position 'i' of the Lista.
     *
     * @param i    Is the number of the position where we want to set the new value.
     * @param info Is the object that we want to save at position 'i'.
     */
    public void set(int i, T info) {
        this.validatePosition(i);
        this.getPos(i).setInfo(info);
    }

    /**
     * Gets the size of the Lista.
     *
     * @return The current size of the Lista.
     */
    public int getSize() {
        return size;
    }

    /**
     * Checks if the Lista is empty.
     *
     * @return true if the Lista is empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.size == 0;
    }

    /*
     * This method will disconnect all the elements of the SimpleList to delete all
     * the content
     */
    public void Clear() {
        this.head = null;
        this.size = 0;
    }

    /**
     * This method will be delete an element from the list, disconnecting their
     * nodes and reconect to fill the space
     * 
     * @param i The position to delete
     */
    public void delete(int i) {
        this.validatePosition(i);
        if (i == 1) {
            Nodo<T> a = head.getNext();
            head.setNext(head.getNext());
            this.head = a;
            size--;
            return;
        } else if (i == size) {
            Nodo<T> a = getPos(size - 1);
            getPos(size).setNext(getPos(size));
            a.setNext(null);
            size--;
            return;
        }
        Nodo<T> a = this.getPos(i - 1);
        Nodo<T> b = this.getPos(i + 1);
        a.setNext(b);
        size--;
    }

    /**
     * This method will return the biggest element into the SimpleList
     * 
     * @return The element with the biggest value
     */
    public T getBiggest() {
        return this.getBiggestNodo().getInfo();
    }

    /**
     * This method will return the Lowest element into the SimpleList
     * 
     * @return The element with the Lowest value
     */
    public T getLowest() {
        return this.getLowestNodo().getInfo();
    }

    /**
     * This method validates if the SimpleList contains an especific element,
     * according to the parameter imputed
     * 
     * @param objeto Receive the element to compare
     * @return Return 'True' if the element exist in the SimpleList, 'False' else
     */
    public boolean contains(T object) {
        if (object == null || this.isEmpty()) {
            String str = "Can't search the element because the object is null or the SimpleList is null too.";
            throw new RuntimeException(str);
        }
        for (Nodo<T> i = this.head; i != null; i = i.getNext()) {
            if (i.getInfo().equals(object)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method will add an element, by default, at the end of the list
     * 
     * @param info The element to add
     */
    public void add(T info) {
        this.addEnd(info);
    }

    /**
     * This method will define the head with the param info, and repoint the Nodes,
     * then info will be the head, and the previous head will be the next of info
     * 
     * @param info It's the object that we wanna save in the init of the Lista
     */
    @SuppressWarnings({ "unchecked" })
    public void addInit(T info) {
        Nodo<T> newNode = new Nodo(info, this.head);
        this.head = newNode;
        this.size++;

    }

    /**
     * This method will find the last Node, and with the param info, will repoint
     * the Nodes, then info will be the previous Node to null, and the previous end
     * will be the previous of info
     * 
     * @param info It's the object that we wanna save in the end of the Lista
     */
    @SuppressWarnings({ "unchecked" })
    public void addEnd(T info) {
        if (this.isEmpty()) {
            this.addInit(info);
        } else {
            Nodo<T> x = getPos(this.size);
            Nodo<T> newNode = new Nodo(info, null);
            x.setNext(newNode);
            this.size++;
        }
    }

    /**
     * This method will add an element in a sorted way, following the parameter for
     * ASCENDENT or DESCENDENT order. This only apply in the fir secuency where the
     * order is correct, and the rest will be ignored
     * 
     * @param info The info/Element to put sorted in the list
     * @param sort The enum that defines what kind of sort is requeried
     */
    public void addSorted(T info, SortedBy sort) {
        if (this.size == 0) {
            this.addInit(info);
            return;
        }
        switch (sort) {
            case SortedBy.ASCENDENT:
                addAscendent(info);
                break;
            case SortedBy.DESCENDENT:
                addDescendent(info);
                break;
            default:
                break;
        }
    }

    /**
     * This method will insert an element inside the position parameter and will
     * move the other elements to the right
     * 
     * @param pos     The position to add the element
     * @param element
     */
    public void add(int pos, T element) {
        if (pos == 1) {
            this.addInit(element);
            return;
        } else if (pos == this.size + 1) {
            this.addEnd(element);
            return;
        }
        validatePosition(pos);
        this.APL(pos, element);
    }

    // ############################################
    // Privates methods used for another methods
    // ############################################

    /**
     * Gets the Node at the specified position, where the Head is the position 1,
     * and the last element is the Node ubicated at the position Size;
     *
     * @param pos The position of the Node we want to get.
     * @return The Node at the specified position.
     */
    private Nodo<T> getPos(int pos) {
        validatePosition(pos);
        Nodo<T> temp = this.head;
        for (int i = 1; i < pos; i++) {
            temp = temp.getNext();
        }
        return temp;
    }

    /**
     * Validates that the specified position is within the bounds of the SimpleList.
     *
     * @param i The position to validate.
     * @throws RuntimeException If the position is out of bounds.
     */
    private void validatePosition(int i) {
        if (i <= 0 || i > this.size) {
            StringBuilder msg = new StringBuilder();
            msg.append("POSITION ERROR: The position " + i + " is outside the edge or is 0\n");
            msg.append("\t\t\t\t\t\t\t");
            msg.append("(border [ 1 -> " + this.size + " ]), so, is not accesible");
            throw new RuntimeException(msg.toString());
        }
    }

    /**
     * This method APL (Add for Position in List) is a util tool that will add an
     * element to a determinated position, but this will only be executed by the
     * principal method add, after validate if exist and special case to mind
     * 
     * @param i       The position where put the element
     * @param element The element to put
     */
    private void APL(int i, T element) {
        Nodo<T> info = new Nodo<T>(element, null);
        Nodo<T> a = this.head;
        Nodo<T> b = this.getPos(i);
        while (a.getNext() != b) {
            a = a.getNext();
        }
        a.setNext(info);
        info.setNext(b);
        this.size++;
    }

    /**
     * Inserts the object info in an ordered (Lower to Highter) position within the
     * SimpleList. [-∞ → ∞]
     *
     * @param info It's the object that we want to insert in an ordered manner.
     */
    @SuppressWarnings("unchecked")
    private void addDescendent(T info) {
        Nodo<T> x = new Nodo<T>(info, null);
        Nodo<T> a = this.head;
        Nodo<T> b = this.head;
        if (x.getInfo().compareTo(this.getBiggest()) < 0) {
            if (size == 1) {
                this.EVDscdnt(x);
                return;
            } else {
                while (a != null) {
                    if (a.getInfo().compareTo(x.getInfo()) < 0) {
                        a = a.getNext();
                        continue;
                    }
                    break;
                }
                while (b != a && b.getNext() != a) {
                    b = b.getNext();
                }
                if (a == b) {
                    this.addInit(info);
                } else {
                    b.setNext(x);
                    x.setNext(a);
                }
            }
        } else {
            add(info);
        }
    }

    /**
     * Inserts the object info in an ordered (Highter to Lower) position within the
     * SimpleList. [∞ → -∞]
     *
     * @param info It's the object that we want to insert in an ordered manner.
     */
    @SuppressWarnings("unchecked")
    private void addAscendent(T info) {
        Nodo<T> x = new Nodo<T>(info, null);
        Nodo<T> a = this.head;
        Nodo<T> b = this.head;
        if (x.getInfo().compareTo(this.getLowest()) > 0) {
            if (size == 1) {
                this.EVAscdnt(x);
                return;
            } else {
                while (a != null) {
                    if (a.getInfo().compareTo(x.getInfo()) > 0) {
                        a = a.getNext();
                        continue;
                    }
                    break;
                }
                while (b != a && b.getNext() != a) {
                    b = b.getNext();
                }
                if (a == b) {
                    this.addInit(info);
                } else {
                    b.setNext(x);
                    x.setNext(a);
                }
            }
        } else {
            add(info);
        }
    }

    /**
     * This method EVDscfnt (Evaluate Value Descendent) is used by [addAscendent()
     * and addDescendent() public methods to evaluate when the SimpleList has only
     * one element inside]
     * 
     * @param x The nodo<T> to evaluate
     */
    @SuppressWarnings("unchecked")
    private void EVDscdnt(Nodo<T> x) {
        if (x.getInfo().compareTo(head.getInfo()) < 0) {
            addInit(x.getInfo());
        } else if (x.getInfo().compareTo(head.getInfo()) > 0) {
            addEnd(x.getInfo());
        }
    }

    /**
     * This method EVAscfnt (Evaluate Value Ascendent) is used by [addAscendent()
     * and addDescendent() public methods to evaluate when the SimpleList has only
     * one element inside]
     * 
     * @param x The nodo<T> to evaluate
     */
    @SuppressWarnings("unchecked")
    private void EVAscdnt(Nodo<T> x) {
        if (x.getInfo().compareTo(head.getInfo()) < 0) {
            addEnd(x.getInfo());
        } else if (x.getInfo().compareTo(head.getInfo()) > 0) {
            addInit(x.getInfo());
        }
    }

    /**
     * This method will return the Nodo<T> that will have the biggest value inside
     * the SimpleList
     * 
     * @return The Biggest Value of the Nodo<T>
     */
    @SuppressWarnings("unchecked")
    private Nodo<T> getBiggestNodo() {
        Nodo<T> a = this.head;
        Nodo<T> b = this.head;
        while (b != null) {
            if (a.getInfo().compareTo(b.getInfo()) < 0) {
                a = b;
            }
            b = b.getNext();
        }
        return a;
    }

    /**
     * This method will return the Nodo<T> that will have the lowest value inside
     * the SimpleList
     * 
     * @return The Biggest Value of the Nodo<T>
     */
    @SuppressWarnings("unchecked")
    private Nodo<T> getLowestNodo() {
        Nodo<T> a = this.head;
        Nodo<T> b = this.head;
        while (b != null) {
            if (a.getInfo().compareTo(b.getInfo()) > 0) {
                a = b;
            }
            b = b.getNext();
        }
        return a;
    }

    /**
     * Returns a string representation of the SimpleList.
     *
     * @return A string that represents the SimpleList.
     */
    @Override
    public String toString() {
        Nodo<T> x = this.head;
        StringBuilder msg = new StringBuilder();
        while (x != null) {
            msg.append(x.getInfo().toString()).append(" -> ");
            x = x.getNext();
        }
        return "[Head] -> " + msg.toString() + "null \n Contains:" + this.size + " elements";
    }
}
