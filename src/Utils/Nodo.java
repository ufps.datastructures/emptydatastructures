package Utils;

/**
 * Dynamic container for any Object
 * 
 * @author Adolfo Alejandro Arenas Ramos
 * @param T the generic object class that we'll save
 */
class Nodo<T> {

    /**
     * This element is contained by the Node
     */
    private T element;

    /**
     * This node has a next node, and this is
     */
    private Nodo<T> next;

    /**
     * Empty default constructor for the Simple Node
     */
    Nodo() {
    }

    /**
     * The constructor for the Simple Node that will save the next Node of this, and
     * the information or generic Object to keep
     * 
     * @param object The generic Object
     * @param next   The next Node
     */
    Nodo(T object, Nodo<T> next) {
        this.element = object;
        this.next = next;
    }

    /**
     * Get the object contained by this Node
     * 
     * @return Return a generic Object
     */
    T getInfo() {
        return this.element;
    }

    /**
     * Get the next Node from this Node
     * 
     * @return Return the next Node
     */
    Nodo<T> getNext() {
        return this.next;
    }

    /**
     * Set the object or generic element contained by this Node for another
     * information or another object
     * 
     * @param object Receive the information or object that we'll put inside this
     *               Node to replace
     */
    void setInfo(T object) {
        this.element = object;
    }

    /**
     * Set the pointer that guide to the next Node to another Node that you want
     * 
     * @param next Receive the Node that you want be the next of this
     */
    void setNext(Nodo<T> next) {
        this.next = next;
    }
}
