package Utils.ItsNecesaryReview;

/**
 *
 * @author docente
 */
public class Pila<T extends Comparable> {

    // decorator:
    private ListaCD<T> tope = new ListaCD();

    public Pila() {
    }

    public void push(T info) {
        this.tope.insertarInicio(info);
    }

    public T pop() {
        return this.tope.remove(0);
    }

    public boolean isVacia() {
        return this.tope.isEmpty();
    }

    public int size() {
        return this.tope.getSize();
    }
}

