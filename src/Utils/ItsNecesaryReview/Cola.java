package Utils.ItsNecesaryReview;

/**
 *
 * @author docente
 */
public class Cola<T extends Comparable> {

    private ListaCD<T> tope = new ListaCD<>();

    public Cola() {
    }

    public void enColar(T info) {
        this.tope.insertarFin(info);
    }

    public T deColar() {
        return this.tope.remove(0);
    }

    public boolean isVacia() {
        return this.tope.isEmpty();
    }

    public int size() {
        return this.tope.getSize();
    }

    @Override
    public String toString() {

        Cola<T> aux = this;

        StringBuffer all = new StringBuffer();

        all.append("Inicio -> ");

        while (aux.size() > 0) {
            T element = aux.deColar();
            all.append(element.toString() + " -> ");
            this.enColar(element);
        }

        return all.toString();
    }
}
