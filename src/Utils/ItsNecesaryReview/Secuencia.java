package Utils.ItsNecesaryReview;

import java.util.Arrays;

/**
 * Static data Structure
 *
 * @author Adolfo Alejandro Arenas Ramos
 * @param <T> It's the type of the Comparable Object that we'll save in our
 *            Secuencia
 */
public class Secuencia<T extends Comparable<T>> {

    private T[] vector;

    /**
     * Empty constructor for a new Secuencia<T>
     */
    public Secuencia() {
    }

    /**
     * Constructor that will replace the internal vector with a new vector
     * 
     * @param vector Receive a <T>Array that will replace the internal Secuencia<T>
     *               Array
     */
    @SuppressWarnings("unchecked")
    public Secuencia(Object[] vector) {
        this.vector = (T[]) vector;
    }

    /**
     * Constructor that will create a new Secuencia<T>, where the internal <T>Array
     * will be initialized with a init size. Doesn't work if the 'n' param is lower
     * or equal to 0
     * 
     * @param n Will be the final size of the internal <T>Array
     */
    @SuppressWarnings("unchecked")
    public Secuencia(int n) {
        if (n <= 0) {
            throw new RuntimeException(
                    "NUMBER ERROR: The Value in the Secuencia<T> needs to be more than 0, but was " + n);
        }
        this.vector = (T[]) new Comparable[n];
    }

    /**
     * This method will add a new Generic<T> Comparable Object in the position 'i'
     * of the internal Array
     * 
     * @param i       Position
     * @param element The element that we'll add
     */
    public void add(int i, T element) {
        this.validate(i);
        this.vector[i] = element;
    }

    /**
     * This method will edit a Generic<T> Comparable Object in the position 'i' of
     * the internal Array
     * 
     * @param i       Position
     * @param element The element that we'll put in the position
     */
    public void set(int i, T element) {
        this.add(i, element);
    }

    /**
     * This method will obtain the Generic<T> Comparable Object in the position 'i'
     * of the internal Array
     * 
     * @param i The position we want to know
     * @return Return the Generic<T> Comparable Object of the position
     */
    public T get(int i) {
        this.validate(i);
        return this.vector[i];
    }

    /**
     * This method will obtain the size of the internal Array and will get the value
     * 
     * @return Return the value of the '.length' for the Array
     */
    public int size() {
        return this.vector.length;
    }

    /**
     * This method will sort the Generic<T> Comparable Objects in the internal Array
     * from the shortest to the biggest, in function of their owns compareTo()
     * methods.
     */
    public void sort() {
        T[] ArrayN = this.vector;
        for (int i = 0; i < ArrayN.length - 1; i++) {
            for (int j = 0; j < ArrayN.length - 1; j++) {
                int c = ArrayN[j].compareTo(ArrayN[j + 1]);
                if (c > 0) {
                    T temp = ArrayN[j + 1];
                    ArrayN[j + 1] = ArrayN[j];
                    ArrayN[j] = temp;
                }
            }
        }
    }

    /**
     * Obtain the lowest element in the internal Array
     * 
     * @return Return the element with the lowest value
     */
    public T getLowest() {
        if (this.getVector() == null) {
            System.out.println("ERROR QUANTITY: The vector for this Secuencia is null.");
        }
        Secuencia<T> x = new Secuencia<T>(this.vector);
        x.sort();
        return x.get(0);
    }

    /**
     * Obtain the biggest element in the internal Array
     * 
     * @return Return the element with the biggest value
     */
    public T getBiggest() {
        if (this.getVector() == null) {
            System.out.println("ERROR QUANTITY: The vector for this Secuencia is null.");
        }
        Secuencia<T> x = new Secuencia<T>(this.vector);
        x.sort();
        return x.get(this.size() - 1);
    }

    /**
     * This method will generate a new Secuencia with the elements of the local
     * Secuencia and another external Secuencia, merging both Secuencia's
     * 
     * @param vector2 Receive the external secuencia to unit
     * @return Return a new Secuencia
     */
    public Secuencia<T> getUnion(Secuencia<T> vector2) {
        Secuencia<T> local = this.skipRepeats();
        Secuencia<T> external = vector2.skipRepeats();

        Secuencia<T> union = new Secuencia<T>(
                local.size() + external.size() - local.numberOfCommonElements(external));
        int x = 0;
        for (int i = 0; i < local.size(); i++) {
            if (local.get(i) != null) {
                union.add(x, local.get(i));
                x++;
            }
        }
        for (int i = 0; i < external.size(); i++) {
            if (!exist(external.get(i), local.getVector())) {
                union.add(x, external.get(i));
                x++;
            }

        }
        return union;
    }

    /**
     * This method will generate a new Secuencia with the elements of the local
     * Secuencia and another external Secuencia, excluding the no-common elements
     * 
     * @param vector2 Receive the external secuencia to intersect
     * @return Return a new Secuencia
     */
    public Secuencia<T> getIntersect(Secuencia<T> vector2) {
        Secuencia<T> local = this.skipRepeats();
        Secuencia<T> external = vector2.skipRepeats();
        if (local.numberOfCommonElements(external) == 0) {
            throw new RuntimeException("There are no common elements between both Secuencia's");
        }
        Secuencia<T> intersection = new Secuencia<T>(local.numberOfCommonElements(external));
        int x = 0;
        for (int i = 0; i < local.size(); i++) {
            for (int j = 0; j < external.size(); j++) {
                if (local.get(i).equals(external.get(j))) {
                    intersection.add(x, local.get(i));
                    x++;
                }
            }
        }
        return intersection;
    }

    /**
     * This method will generate a new Secuencia with the elements of the local
     * Secuencia and another external Secuencia, excluding all the elements of the
     * external Secuencia compared
     * 
     * @param vector2 Receive the external secuencia to exclude the common space
     * @return Return a new Secuencia
     */
    public Secuencia<T> getDiference(Secuencia<T> vector2) {
        Secuencia<T> local = this.skipRepeats();
        Secuencia<T> external = vector2.skipRepeats();
        if (local.numberOfCommonElements(external) == local.size())
            throw new RuntimeException("There are no common elements between both Secuencia's");
        Secuencia<T> diference = new Secuencia<T>(local.size() - local.numberOfCommonElements(external));
        int x = 0;
        for (int i = 0; i < local.size(); i++) {
            if (!exist(local.get(i), external.getVector())) {
                diference.add(x, local.get(i));
                x++;
            }
        }
        return diference;
    }

    /**
     * This method will generate a new Secuencia with the elements of the local
     * Secuencia and another external Secuencia, excluding only the common elements
     * and saving the no-common
     * 
     * @param vector2 Receive the external secuencia to exclude the common space
     * @return Return a new Secuencia
     */
    public Secuencia<T> getSimetricDiference(Secuencia<T> vector2) {
        Secuencia<T> a_b = new Secuencia<T>(this.getDiference(vector2).getVector());
        Secuencia<T> b_a = new Secuencia<T>(vector2.getDiference(this).getVector());
        if (a_b.size() == 0 && b_a.size() == 0)
            throw new RuntimeException("There are no Simetric Diference between both Secuencia's");
        Secuencia<T> simetricDiference = new Secuencia<T>(a_b.size() + b_a.size());
        int x = 0;
        for (int i = 0; i < a_b.size(); i++) {
            simetricDiference.add(x, a_b.get(i));
            x++;
        }
        for (int i = 0; i < b_a.size(); i++) {
            simetricDiference.add(x, b_a.get(i));
            x++;
        }
        return simetricDiference;
    }

    /**
     * This method will create a new Secuencia of Secuencia's of Generic<T>
     * Comparable Object, where each internal Secuencia will contain a serie of
     * elements that will depends by the number of elements in the original
     * Secuencia. These are the Even Sets of the original Secuencia
     * 
     * @return Return a Secuencia of Secuencia's
     */
    public Secuencia<T> getEvenSets() {
        Secuencia<T> evenSets = new Secuencia<T>();
        evenSets.convertToSecuenciaOfSecuencia(createSecuenciaArray());
        return evenSets;
    }

    /**
     * This method will create a new Secuencia of Generic<T> Comparable Objects,
     * without the repeated elements of the original Secuencia
     * 
     * @return Return a Secuencia without repeated elements
     */
    public Secuencia<T> skipRepeats() {
        Secuencia<T> secuenciaTemp = new Secuencia<>(this.vector.length);
        int x = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (i == 0) {
                secuenciaTemp.add(i, this.vector[x]);
                x++;
            }
            if (!exist(this.vector[i], secuenciaTemp.getVector())) {
                secuenciaTemp.add(x, this.vector[i]);
                x++;
            }
        }
        Secuencia<T> noRepeated = new Secuencia<>(secuenciaTemp.size() - secuenciaTemp.emptySpaces());
        for (int i = 0; i < noRepeated.size(); i++) {
            if (secuenciaTemp.get(i) != null) {
                noRepeated.add(i, secuenciaTemp.get(i));
            }
        }
        return noRepeated;
    }

    /**
     * This method will create a new Array of Secuencia[]'s where each space will be
     * one of the Even Sets
     * 
     * @return Return an Array of Secuencia's
     */
    @SuppressWarnings("unchecked")
    public Secuencia<T>[] createSecuenciaArray() {

        Secuencia<T>[] arrayPotencia = new Secuencia[(int) (Math.pow(2, size()))];
        int x = 0;
        for (int i = 0; i < size(); i++) {
            Secuencia<T> arrayTemp = new Secuencia<>((T[]) new Comparable[] { this.vector[i] });
            for (int j = 0; j < arrayPotencia.length; j++) {
                if (arrayPotencia[j] == null)
                    continue;
                Secuencia<T> addTempo = arrayTemp.getUnion((arrayPotencia[j]));
                if (!addTempo.equals(arrayPotencia[j])) {
                    arrayPotencia[x] = addTempo;
                    x++;
                }
            }
            arrayPotencia[x] = arrayTemp;
            x++;
        }
        this.printPares(arrayPotencia);
        return arrayPotencia;
    }

    /**
     * This method return the internal Array of the Secuencia
     * 
     * @return
     */
    public T[] getVector() {
        return this.vector;
    }

    /**
     * This method will compare across three Security Clausules if two Secuencia's
     * are equal
     * 
     * @return Return 'true' if this Secuencia and another object are both instances
     *         of Secuencia and their internal Arrays have the same values
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (this.getClass() == obj.getClass()) {
                Secuencia<T> other = (Secuencia<T>) obj;
                if (Arrays.equals(this.getVector(), other.getVector())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method will be used to print or show in a personalized form the info
     * inside the Secuencia
     */
    @Override
    public String toString() {
        String txt = "";
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] != null)
                txt += this.vector[i] + ", ";
            else
                txt += "null\n";
        }
        return txt;
    }

    // ############################################
    // Privates methods used for another methods
    // ############################################

    /**
     * This PRIVATE METHOD will evaluate if the position 'i' in the internal Array
     * is right.
     * IT'S NECCESARY FOR get(int) and add(T)
     * 
     * @param i The position that we'll evaluate
     */
    private void validate(int i) {
        if (i < 0 || i >= this.vector.length) {
            throw new RuntimeException(
                    "NUMBER ERROR: The position '" + i + "' in the internal Array of the Secuencia doesn't exist");
        }
    }

    /**
     * This PRVATE METHOD will change the internal Array of the Secuencia, for a new
     * Secuencia<T> Array, then, we'll have a Secuencia of Secuencia's
     * IT'S NECCESARRY FOR getEvenSets()
     * 
     * @param secuencia Receive the Secuencia[] Array
     */
    private void convertToSecuenciaOfSecuencia(Secuencia<T>[] secuencia) {
        @SuppressWarnings("unchecked")
        T[] genericArray = (T[]) new Comparable[secuencia.length];
        for (int i = 0; i < secuencia.length; i++) {
            if (secuencia[i] == null) {
                genericArray[i] = null;
                continue;
            }
            genericArray[i] = secuencia[i].getVector()[0];
        }
        this.vector = genericArray;
    }

    /**
     * This method will test if and determinated Generic<T> Comparable Object exist
     * in the internal Array of the Secuenia
     * IT'S NECCESARY IN getDiference(), getUnion(), skipRepeats()
     * 
     * @param element Receive the Generic<T> Comparable Object to check
     * @param vector  Receive the external Array where we'll check for the element
     * @return Return 'true' if the element already exist in the vector, 'false' if
     *         don't
     */
    private boolean exist(T element, T[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == element)
                return true;
        }
        return false;
    }

    /**
     * This method will count each element in the local Secuencia that be equal to
     * another in an external Secuencia
     * IT'S NECCESARY IN getDiference(), getIntersect(), getUnion()
     * 
     * @param vector2 Receive an external Secuencia to compare
     * @return Return the number of same elements between both Secuencia's
     */
    public int numberOfCommonElements(Secuencia<T> vector2) {
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = 0; j < vector2.getVector().length; j++) {
                if (this.vector[i].equals(vector2.getVector()[j])) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * This method will count each space in the internal Array and will plus 1 by
     * each null or empty space
     * 
     * @return Return the number of null or empty elements
     */
    private int emptySpaces() {
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] == null) {
                count++;
            }
        }
        return count;
    }

    /**
     * This method will print the even sets generated from a Secuencia[] Array
     * 
     * @param secuencias receive a Array of Secuencia's
     */
    private void printPares(Secuencia<T>[] secuencias) {
        for (int i = 0; i < secuencias.length; i++) {
            System.out.print("{");
            if (secuencias[i] == null) {
                System.out.println("null}");
                continue;
            }
            for (int j = 0; j < secuencias[i].size(); j++) {
                if (secuencias[i].size() == 1) {
                    System.out.print(secuencias[i].get(j));
                } else if (j == secuencias[i].size() - 1) {
                    System.out.print(secuencias[i].get(j));
                } else
                    System.out.print(secuencias[i].get(j) + ",");
            }
            System.out.print("}\n");
        }
    }
}
