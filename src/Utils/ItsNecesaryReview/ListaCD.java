package Utils.ItsNecesaryReview;

/**
 *
 * @author docente
 */
@SuppressWarnings("rawtypes")
public class ListaCD<T extends Comparable> {

    private NodoD<T> centinela;
    private int size = 0;

    @SuppressWarnings({ "unchecked" })
    public ListaCD() {
        this.centinela = new NodoD();
        this.centinela.setAnt(centinela);
        this.centinela.setSig(centinela);
    }

    @SuppressWarnings({ "unchecked" })
    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.centinela.getSig(), this.centinela);
        this.centinela.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    @SuppressWarnings({ "unchecked" })
    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.centinela, this.centinela.getAnt());
        nuevo.getAnt().setSig(nuevo);
        this.centinela.setAnt(nuevo);
        this.size++;
    }

    public String toString() {
        if (this.isEmpty()) {
            return "Empty list";
        }
        String msg = "\n --> ";
        for (NodoD<T> aux = this.centinela.getSig(); aux != this.centinela; aux = aux.getSig()) {
            msg += aux.getInfo().toString() + " --> ";
            if (aux.getSig() == this.centinela)
                msg += "";
        }

        return msg;
    }

    public boolean isEmpty() {
        return this.centinela == this.centinela.getSig() || this.centinela == null || this.size == 0;
    }

    public int getSize() {
        return size;
    }

    public T get(int i) {
        return this.getPos(i).getInfo();
    }

    public void set(int i, T elemento) {
        this.getPos(i).setInfo(elemento);
    }

    public void vaciarLista() {
        this.centinela.setAnt(centinela);
        this.centinela.setSig(centinela);
    }

    public T remove(int i) {

        try {
            NodoD<T> borrar = this.getPos(i);
            borrar.getAnt().setSig(borrar.getSig());
            borrar.getSig().setAnt(borrar.getAnt());
            borrarNodo(borrar);
            this.size--;
            return borrar.getInfo();

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    private void borrarNodo(NodoD<T> x) {
        x.setSig(x);
        x.setAnt(x);
        this.size--;
    }

    private NodoD<T> getPos(int i) {
        this.validarPos(i);
        // Referenciar, no crear
        NodoD<T> pos = this.centinela.getSig();
        for (int j = 0; j < i; j++) {
            pos = pos.getSig();
        }
        return pos;
    }

    private void validarPos(int i) {
        if (this.isEmpty() || i < 0 || i >= this.size) {
            throw new RuntimeException("Indice: " + i + " fuera de Rango");
        }
    }

    @SuppressWarnings("unchecked")
    public ListaCD<T> getMenores() {

        ListaCD<T> menores = new ListaCD<>();

        NodoD<T> menor = this.centinela.getSig();

        NodoD<T> iterator = this.centinela.getSig();

        while (iterator.getSig() != this.centinela) {

            while (menor.getInfo().compareTo(iterator.getInfo()) > 0) {
                menor = iterator;
                menores.vaciarLista();
            }

            if (menor.getInfo().equals(iterator.getInfo())) {
                menores.insertarInicio(menor.getInfo());
            }
            iterator = iterator.getSig();
        }
        return menores;
    }

    public void borrarRepetidos() {

        NodoD<T> iterator = this.centinela.getSig();

        while (iterator != centinela) {

            NodoD<T> aux = iterator.getSig();

            while (aux != this.centinela) {
                if (iterator.getInfo().equals(aux.getInfo())) {

                    aux.getSig().setAnt(aux.getAnt());
                    aux.getAnt().setSig(aux.getSig());

                    NodoD<T> borrar = aux;
                    aux = aux.getSig();
                    borrarNodo(borrar);
                    continue;
                }
                aux = aux.getSig();
            }
            iterator = iterator.getSig();
        }
    }

    @SuppressWarnings("unchecked")
    public void sort() {

        if (this.isEmpty()) {
            System.out.println("La lista no tiene elementos para ordenar");
            return;
        }

        NodoD<T> iterator = this.centinela.getSig().getSig();

        while (iterator != this.centinela) {
            NodoD<T> key = iterator;
            NodoD<T> prev = iterator.getAnt();

            key.getAnt().setSig(key.getSig());
            key.getSig().setAnt(key.getAnt());

            while (prev != this.centinela && key.getInfo().compareTo(prev.getInfo()) < 0) {
                prev = prev.getAnt();
            }

            key.setAnt(prev);
            key.setSig(prev.getSig());
            prev.getSig().setAnt(key);
            prev.setSig(key);

            iterator = key.getSig();
        }
    }

}
