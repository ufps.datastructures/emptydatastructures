# *EmptyDataStructures*

## Name

This project will save all the data Structure tha I saw in my academic Data Structures University Course, in the Francisco de Paula Santander University.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage

These codes are just for academic and personal uses and doesn't represent a more efficient or practical solution for any logical or technical problem.

## Support

For support or any explain about the codes inside this repository, contact me to this e-mail:

* adolfoalejandroar@ufps.edu.co

Or, you can also contact me to my personal e-mail:

* Adolfoalejandroarenasramos@hotmail.com

## License

These codes have no licenses about their usage.

## Project status

This project is in developing and could be updated or not in the future.

## Author

Adolfo Alejandro Arenas Ramos ~ San José de Cúcuta, Colombia

Francisco de Paula Santander University
